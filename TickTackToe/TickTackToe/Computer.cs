﻿using System.Collections.Generic;
using System.Linq;

namespace TickTackToe
{
  class Computer : Player
  {
    enum Cell
    {
      Null,
      Computer,
      Person
    }

    private Dictionary<string, int> variants;
    private int[,] veights;
    private Cell[,] figures;

    public Computer(bool isCross) : base(isCross)
    {
      variants = Constants.Variants;
    }

    public void Go()
    {
      FillFigures();
      SetVeights();

      int maxVeight = 0;
      int x = 0, y = 0;

      for (int i = 0; i < Constants.NumberOfFigures; i++)
      {
        for (int j = 0; j < Constants.NumberOfFigures; j++)
        {
          if (maxVeight < veights[i, j])
          {
            maxVeight = veights[i, j];
            x = i;
            y = j;
          }
        }
      }

      Figures.Add(new Figure(x, y));
    }

    private void FillFigures()
    {
      figures = new Cell[Constants.NumberOfFigures, Constants.NumberOfFigures];

      foreach (var figure in Game.person.Figures)
      {
        figures[figure.X, figure.Y] = Cell.Person;
      }

      foreach (var figure in Figures)
      {
        figures[figure.X, figure.Y] = Cell.Computer;
      }
    }

    private void SetVeights()
    {
      veights = new int[Constants.NumberOfFigures, Constants.NumberOfFigures];

      for (int x = 0; x < Constants.NumberOfFigures; x++)
      {
        for (int y = 0; y < Constants.NumberOfFigures; y++)
        {
          if (figures[x, y] != Cell.Null)
          {
            continue;
          }

          veights[x, y] = 0;

          SetVeights(x, y, 0, 1);
          SetVeights(x, y, 0, -1);
          SetVeights(x, y, 1, 0);
          SetVeights(x, y, -1, 0);
          SetVeights(x, y, 1, 1);
          SetVeights(x, y, -1, 1);
          SetVeights(x, y, 1, -1);
          SetVeights(x, y, -1, -1);
        }
      }
      return;
    }

    private void SetVeights(int x, int y, int dX, int dY)
    {
      SetDef(x, y, dX, dY);
      SetOff(x, y, dX, dY);
    }

    private void SetDef(int x, int y, int dX, int dY)
    {
      string str = "0";

      for (int counter = 1; counter < Constants.CountToWin; counter++)
      {
        if (IsValid(x + dX * counter, y + dY * counter) && figures[x + dX * counter, y + dY * counter] == Cell.Null)
        {
          str += "0";
        }
        else if (IsValid(x + dX * counter, y + dY * counter) && figures[x + dX * counter, y + dY * counter] == Cell.Person)
        {
          str += "1";
        }
        else
        {
          break;
        }
      }

      int add = 0;

      if (str.Length == Constants.CountToWin)
      {
        add = variants[str];
      }

      if (Game.person.Figures.Any(f => !(f.X == x && f.Y == y) && (f.X == x + 1 || f.X == x - 1 || f.X == x) && (f.Y == y - 1 || f.Y == y + 1 || f.Y == y)))
      {
        add *= 4;
      }

      veights[x, y] += add;

      return;
    }

    private void SetOff(int x, int y, int dX, int dY)
    {
      int offset = 0;
      while (IsValid(x - dX, y - dY) && figures[x - dX, y - dY] != Cell.Person && offset != Constants.CountToWin)
      {
        x -= dX;
        y -= dY;
        offset++;
      }

      int max = 0;
      for (int l = 0; l < offset + 1; l++)
      {
        string str = "";

        for (int k = 0; k < Constants.CountToWin; k++)
        {
          if (IsValid(x + dX * k, y + dY * k) && figures[x + dX * k, y + dY * k] != Cell.Person)
          {
            str += (int)figures[x + dX * k, y + dY * k];
          }
        }

        if (str.Length == Constants.CountToWin)
        {
          int add = variants[str] * 10;
          if (str == "00000")
          {
            add /= 10;
          }
          if (Figures.Any(f => !(f.X == x && f.Y == y) && (f.X == x + 1 || f.X == x - 1 || f.X == x) && (f.Y == y - 1 || f.Y == y + 1 || f.Y == y)))
          {
            add *= 4;
          }
          if (max < add)
          {
            max = add;
          }
        }

        if (l != offset)
        {
          x += dX;
          y += dY;
        }
      }

      veights[x, y] += max;

      return;
    }

    private static bool IsValid(int i, int j)
    {
      return (i >= 0 && i < Constants.NumberOfFigures && j >= 0 && j < Constants.NumberOfFigures);
    }

    public override string ToString()
    {
      return "computer";
    }
  }
}
