﻿using System.Collections.Generic;
using ChessMate.Figures;
using ChessMate.Enums;

namespace ChessMate
{
  static class CheckingMate
  {
    public static Move mateMove;

    public static CheckMateResults Check(List<Figure> figures)
    {
      if (KingIsAttacked(figures, Colors.Black) || KingIsAttacked(figures, Colors.White))
      {
        return CheckMateResults.KingIsAttacked;
      }

      for (int figCounter = 0; figCounter < figures.Count; figCounter++)
      {
        // Рассматриваем только белые фигуры
        if (figures[figCounter].Color == Colors.Black)
        {
          continue;
        }

        // Определяем все возможные ходы белой фигуры
        List<Figure> allPossibleMovesOfWhiteFigure = figures[figCounter].PossibleMoves(figures);
        Figure lastPositionOfWhiteFigure = figures[figCounter];

        CheckMateResults result = CheckAllWhiteMoves(figures, figCounter, allPossibleMovesOfWhiteFigure, lastPositionOfWhiteFigure);

        if (result != CheckMateResults.NoMate)
        {
          return result;
        }
      }

      return CheckMateResults.NoMate;
    }


    private static CheckMateResults CheckAllWhiteMoves(List<Figure> figures, int figCounter, List<Figure> allPossibleMoves, Figure lastPosition)
    {
      int indexCut = Constants.IsNotCut;

      for (int moveCounter = 0; moveCounter < allPossibleMoves.Count; moveCounter++)
      {
        mateMove = new Move(lastPosition.ToString(), lastPosition.X, lastPosition.Y, allPossibleMoves[moveCounter].X, allPossibleMoves[moveCounter].Y);
        figures[figCounter] = allPossibleMoves[moveCounter];

        // Определяем срубила ли белая фигура в результате своего хода
        indexCut = FindCut(allPossibleMoves[moveCounter], figures);

        // Проверяем не открылся ли под удар белый король в результате хода своей фигуры
        if (KingIsAttacked(figures, Colors.White))
        {
          continue;
        }

        // Проверяем исключительный случай с пешкой, превращающейся в ферзя или коня
        if (figures[figCounter] is Pawn && figures[figCounter].Y == 0)
        {
          if (IsPawnToQueenOrKnight(figures, allPossibleMoves[moveCounter]))
          {
            RemoveAllChanges(figures, figCounter, lastPosition, indexCut);

            return CheckMateResults.MateWithChangePawn;
          }
        }

        // Следующий этап проверки
        if (CheckShah(figures))
        {
          figures[figCounter] = lastPosition;
          return CheckMateResults.Mate;
        }
      }

      RemoveAllChanges(figures, figCounter, lastPosition, indexCut);
      return CheckMateResults.NoMate;
    }


    private static bool CheckShah(List<Figure> figures)
    {
      King blackKing = FindKing(figures, Colors.Black);

      foreach (var figure in figures)
      {
        // Проверяем под ударом ли черный король в результате хода белой фигуры
        if (figure.Color == Colors.White && figure.CanCut(blackKing, figures))
        {
          return CheckMate(figures);
        }
      }
      return false;
    }


    private static bool CheckMate(List<Figure> figures)
    {
      for (int figCounter = 0; figCounter < figures.Count; figCounter++)
      {
        // Рассматриваем только черные несрубленные фигуры
        if (figures[figCounter].Color == Colors.White || figures[figCounter].IsCut)
        {
          continue;
        }

        // Определяем возможные ходы черной фигуры
        List<Figure> allPossibleMovesOfFigure = figures[figCounter].PossibleMoves(figures);
        Figure lastPositionOfBlackFigure = figures[figCounter];

        if (!CheckAllBlackMoves(figures, figCounter, allPossibleMovesOfFigure, lastPositionOfBlackFigure))
        {
          return false;
        }
      }
      return true;
    }


    private static bool CheckAllBlackMoves(List<Figure> figures, int figCounter, List<Figure> allPossibleMoves, Figure lastPosition)
    {
      int indexCut = Constants.IsNotCut;

      for (int moveCounter = 0; moveCounter < allPossibleMoves.Count; moveCounter++)
      {
        figures[figCounter] = allPossibleMoves[moveCounter];

        // Проверяем срубила ли черная фигура белую в результате своего хода
        indexCut = FindCut(figures[figCounter], figures);

        if (!CheckSavingKing(figures, allPossibleMoves[moveCounter]))
        {
          RemoveAllChanges(figures, figCounter, lastPosition, indexCut);

          // Фигура спасла своего короля, данный ход белой фигуры не привел к мату
          return false;
        }
      }

      RemoveAllChanges(figures, figCounter, lastPosition, indexCut);
      return true;
    }


    private static bool CheckSavingKing(List<Figure> figures, Figure thisBlackFigure)
    {
      King blackKing = FindKing(figures, Colors.Black);

      foreach (var figure in figures)
      {
        // Проверяем по-прежнему ли черный король под ударом
        if (figure.Color == Colors.White && figure.CanCut(blackKing, figures) && !figure.IsCut)
        {
          return true;
        }
      }

      return false;
    }


    private static bool KingIsAttacked(List<Figure> figures, Colors color)
    {
      King king = FindKing(figures, color);

      foreach (var figure in figures)
      {
        if (figure.Color != color && figure.CanCut(king, figures) && !figure.IsCut)
        {
          return true;
        }
      }
      return false;
    }


    private static bool IsPawnToQueenOrKnight(List<Figure> figures, Figure moveOfThisWhiteFigure)
    {
      int index = figures.FindLastIndex(f => f.X == moveOfThisWhiteFigure.X && f.Y == moveOfThisWhiteFigure.Y);
      figures[index] = new Queen(moveOfThisWhiteFigure.X, moveOfThisWhiteFigure.Y, moveOfThisWhiteFigure.Color);

      if (CheckShah(figures))
      {
        return true;
      }

      index = figures.FindLastIndex(f => f.X == moveOfThisWhiteFigure.X && f.Y == moveOfThisWhiteFigure.Y);
      figures[index] = new Knight(moveOfThisWhiteFigure.X, moveOfThisWhiteFigure.Y, moveOfThisWhiteFigure.Color);

      if (CheckShah(figures))
      {
        return true;
      }
      return false;
    }


    public static King FindKing(List<Figure> figures, Colors color)
    {
      King king = new King(0, 0, 0);

      foreach (var figure in figures)
      {
        if (figure is King && figure.Color == color)
        {
          king = (King)figure;
          break;
        }
      }
      return king;
    }


    public static int FindCut(Figure figure, List<Figure> figures)
    {
      for (int i = 0; i < figures.Count; i++)
      {
        if (figures[i] != figure && figures[i].X == figure.X && figures[i].Y == figure.Y)
        {
          figures[i].IsCut = true;
          return i;
        }
      }
      return Constants.IsNotCut;
    }


    private static void RemoveAllChanges(List<Figure> figures, int figCounter, Figure lastPosition, int indexCut)
    {
      if (indexCut != Constants.IsNotCut)
      {
        figures[indexCut].IsCut = false;
      }
      figures[figCounter] = lastPosition;
    }
  }


  struct Move
  {
    public string Figure;
    public int X1;
    public int Y1;
    public int X2;
    public int Y2;
    public Move(string fig, int i, int j, int k, int l)
    {
      Figure = fig;
      X1 = i;
      Y1 = j;
      X2 = k;
      Y2 = l;
    }
  }
}
