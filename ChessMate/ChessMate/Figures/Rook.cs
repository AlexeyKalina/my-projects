﻿using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using ChessMate.Enums;

namespace ChessMate.Figures
{
  class Rook : Figure
  {
    public Rook(int i, int j, Colors col) : base(i, j, col) { }

    public override Bitmap WhiteOnBlack { get; } = Properties.Resources.black_white_rook;

    public override Bitmap WhiteOnWhite { get; } = Properties.Resources.white_white_rook;

    public override Bitmap BlackOnBlack { get; } = Properties.Resources.black_black_rook;

    public override Bitmap BlackOnWhite { get; } = Properties.Resources.white_black_rook;


    public override List<Figure> PossibleMoves(List<Figure> figures)
    {
      List<Figure> allMoves = new List<Figure>();

      allMoves.AddRange(MovesInOneDirection(figures, 1, 0));

      allMoves.AddRange(MovesInOneDirection(figures, -1, 0));

      allMoves.AddRange(MovesInOneDirection(figures, 0, 1));

      allMoves.AddRange(MovesInOneDirection(figures, 0, -1));

      return allMoves;
    }


    public override bool CanCut(Figure figure, List<Figure> figures)
    {
      if (X == figure.X)
      {
        int y1 = Y > figure.Y ? Y : figure.Y;
        int y2 = Y < figure.Y ? Y : figure.Y;

        for (y1--; y1 > y2; y1--)
        {
          if (figures.Any(z => z.Y == y1 && z.X == X))
          {
            return false;
          }
        }
      }

      else if (Y == figure.Y)
      {
        int x1 = X > figure.X ? X : figure.X;
        int x2 = X < figure.X ? X : figure.X;

        for (x1--; x1 > x2; x1--)
        {
          if (figures.Any(z => z.X == x1 && z.Y == Y))
          {
            return false;
          }
        }
      }

      else
      {
        return false;
      }
      return true;
    }

    private List<Figure> MovesInOneDirection(List<Figure> figures, int dX, int dY)
    {
      List<Figure> movesInThisDirection = new List<Figure>();

      for (int x = X + dX, y = Y + dY; IsBound(x, y); x += dX, y += dY)
      {
        if (!figures.Any(z => z.Y == y && z.X == x))
        {
          movesInThisDirection.Add(new Rook(x, y, Color));
        }
        else if (figures.Any(z => z.Y == y && z.X == x && Color != z.Color && !z.IsCut))
        {
          movesInThisDirection.Add(new Rook(x, y, Color));
          break;
        }
        else
        {
          break;
        }
      }

      return movesInThisDirection;
    }

    public override string ToString()
    {
      return "rook";
    }
  }
}
