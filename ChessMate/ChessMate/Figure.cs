﻿using System.Collections.Generic;
using System.Drawing;
using ChessMate.Enums;

namespace ChessMate
{
  public abstract class Figure
  {
    private int _coordX, _coordY;
    private Colors _col;

    protected Figure(int i, int j, Colors color)
    {
      IsCut = false;
      Color = color;
      X = i;
      Y = j;
    }
    public abstract Bitmap WhiteOnBlack { get; }
    public abstract Bitmap WhiteOnWhite { get; }
    public abstract Bitmap BlackOnBlack { get; }
    public abstract Bitmap BlackOnWhite { get; }

    public Colors Color
    {
      get { return _col; }
      set { _col = value; }
    }
    public int X
    {
      get { return _coordX; }
      set { if (value >= 0 && value < Constants.NumberOfCells) _coordX = value; }
    }
    public int Y
    {
      get { return _coordY; }
      set { if (value >= 0 && value < Constants.NumberOfCells) _coordY = value; }
    }
    public bool IsCut { get; set; }

    /// <summary>
    /// Возвращает список всех возможных ходов данной фигуры при заднной расстановке figures
    /// </summary>
    /// <param name="figures">Заданная расстановка фигур</param>
    /// <returns></returns>
    public abstract List<Figure> PossibleMoves(List<Figure> figures);

    /// <summary>
    /// Определяет, может ли figure срубить данную фигуру при заданной расстановке
    /// </summary>
    /// <param name="figure">Фигура, которая должна срубить данную</param>
    /// <param name="figures">Заданная расстановка фигур</param>
    /// <returns></returns>
    public abstract bool CanCut(Figure figure, List<Figure> figures);

    protected bool IsBound(int x, int y)
    {
      return x >= 0 && y >= 0 && x < Constants.NumberOfCells && y < Constants.NumberOfCells;
    }
  }
}
