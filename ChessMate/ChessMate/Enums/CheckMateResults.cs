﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChessMate.Enums
{
  enum CheckMateResults
  {
    NoMate,
    Mate,
    MateWithChangePawn,
    KingIsAttacked
  }
}
