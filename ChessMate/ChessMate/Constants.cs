﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace ChessMate
{
  static class Constants
  {
    public const int NumberOfCells = 8;
    public const int NumberOfFigures = 6;

    public const int WhiteKingNumber = 4;
    public const int BlackKingNumber = 10;

    public const int SizeOfImage = 40;

    public const int FreeWhiteCell = -2;
    public const int FreeBlackCell = -1;

    public const int IsNotChosen = -1;
    public const int IsNotCut = -1;

    public static Color White = Color.FromArgb(124, 78, 52);
    public static Color Black = Color.FromArgb(247, 240, 213);

  }
}
