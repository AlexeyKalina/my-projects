﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TickTackToe
{
  struct Figure
  {
    int x;
    int y;

    public Figure(int x, int y)
    {
      this.x = x;
      this.y = y;
    }

    public int X
    {
      get { return x; }
    }
    public int Y
    {
      get { return y; }
    }
  }
}
