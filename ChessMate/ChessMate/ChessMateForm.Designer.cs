﻿namespace ChessMate
{
    partial class ChessMateForm
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ChessMateForm));
            this.checkMateButton = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.gameMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newGameMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.CheckMateMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.referenseMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutProgrameMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // checkMateButton
            // 
            this.checkMateButton.Location = new System.Drawing.Point(165, 428);
            this.checkMateButton.Name = "checkMateButton";
            this.checkMateButton.Size = new System.Drawing.Size(102, 23);
            this.checkMateButton.TabIndex = 0;
            this.checkMateButton.Text = "Проверить мат";
            this.checkMateButton.UseVisualStyleBackColor = true;
            this.checkMateButton.Click += new System.EventHandler(this.checkMateButton_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.gameMenuItem,
            this.referenseMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(464, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // gameMenuItem
            // 
            this.gameMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newGameMenuItem,
            this.CheckMateMenuItem,
            this.exitMenuItem});
            this.gameMenuItem.Name = "gameMenuItem";
            this.gameMenuItem.Size = new System.Drawing.Size(59, 20);
            this.gameMenuItem.Text = "Партия";
            // 
            // newGameMenuItem
            // 
            this.newGameMenuItem.Name = "newGameMenuItem";
            this.newGameMenuItem.Size = new System.Drawing.Size(179, 22);
            this.newGameMenuItem.Text = "Новая расстановка";
            this.newGameMenuItem.Click += new System.EventHandler(this.newGameMenuItem_Click);
            // 
            // CheckMateMenuItem
            // 
            this.CheckMateMenuItem.Name = "CheckMateMenuItem";
            this.CheckMateMenuItem.Size = new System.Drawing.Size(179, 22);
            this.CheckMateMenuItem.Text = "Проверить мат";
            this.CheckMateMenuItem.Click += new System.EventHandler(this.CheckMateMenuItem_Click);
            // 
            // exitMenuItem
            // 
            this.exitMenuItem.Name = "exitMenuItem";
            this.exitMenuItem.Size = new System.Drawing.Size(179, 22);
            this.exitMenuItem.Text = "Выйти";
            this.exitMenuItem.Click += new System.EventHandler(this.exitMenuItem_Click);
            // 
            // referenseMenuItem
            // 
            this.referenseMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutProgrameMenuItem});
            this.referenseMenuItem.Name = "referenseMenuItem";
            this.referenseMenuItem.Size = new System.Drawing.Size(65, 20);
            this.referenseMenuItem.Text = "Справка";
            // 
            // aboutProgrameMenuItem
            // 
            this.aboutProgrameMenuItem.Name = "aboutProgrameMenuItem";
            this.aboutProgrameMenuItem.Size = new System.Drawing.Size(149, 22);
            this.aboutProgrameMenuItem.Text = "О программе";
            this.aboutProgrameMenuItem.Click += new System.EventHandler(this.aboutProgrameMenuItem_Click);
            // 
            // ChessMateForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(464, 482);
            this.Controls.Add(this.checkMateButton);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "ChessMateForm";
            this.Text = "ChessMate";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button checkMateButton;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem gameMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newGameMenuItem;
        private System.Windows.Forms.ToolStripMenuItem CheckMateMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitMenuItem;
        private System.Windows.Forms.ToolStripMenuItem referenseMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutProgrameMenuItem;
    }
}

