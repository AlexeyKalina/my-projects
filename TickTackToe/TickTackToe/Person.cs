﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TickTackToe
{
  class Person : Player
  {
    public Person(bool isCross) : base(isCross) { }

    public void Go(int x, int y)
    {
      Figures.Add(new Figure(x, y));
    }

    public override string ToString()
    {
      return "person";
    }
  }
}
