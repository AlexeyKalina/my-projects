﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using ChessMate.Enums;

namespace ChessMate.Figures
{
  class King : Figure
  {
    public King(int i, int j, Colors col) : base(i, j, col) { }

    public override Bitmap WhiteOnBlack { get; } = Properties.Resources.black_white_king;

    public override Bitmap WhiteOnWhite { get; } = Properties.Resources.white_white_king;

    public override Bitmap BlackOnBlack { get; } = Properties.Resources.black_black_king;

    public override Bitmap BlackOnWhite { get; } = Properties.Resources.white_black_king;


    public override List<Figure> PossibleMoves(List<Figure> figures)
    {
      List<Figure> allMoves = new List<Figure>();

      allMoves.AddRange(MovesInOneDirection(figures, 1, 1));

      allMoves.AddRange(MovesInOneDirection(figures, 1, -1));

      allMoves.AddRange(MovesInOneDirection(figures, -1, 1));

      allMoves.AddRange(MovesInOneDirection(figures, -1, -1));

      allMoves.AddRange(MovesInOneDirection(figures, 1, 0));

      allMoves.AddRange(MovesInOneDirection(figures, -1, 0));

      allMoves.AddRange(MovesInOneDirection(figures, 0, 1));

      allMoves.AddRange(MovesInOneDirection(figures, 0, -1));

      return allMoves;
    }
    

    public override bool CanCut(Figure figure, List<Figure> figures)
    {
      if (Math.Abs(X - figure.X) <= 1 && Math.Abs(Y - figure.Y) <= 1 && Color != figure.Color)
      {
        return true;
      }
      else
      {
        return false;
      }
    }


    private List<Figure> MovesInOneDirection(List<Figure> figures, int dX, int dY)
    {
      List<Figure> moveInThisDirectiion = new List<ChessMate.Figure>();

      if (IsBound(X + dX, Y + dY) && !figures.Any(x => x.X == X + dX && x.Y == Y + dY && Color == x.Color && !x.IsCut))
      {
        moveInThisDirectiion.Add(new King(X + dX, Y + dY, Color));
      }

      return moveInThisDirectiion;
    }

    public override string ToString()
    {
      return "king";
    }
  }
}
