﻿using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using ChessMate.Enums;

namespace ChessMate.Figures
{
  class Pawn : Figure
  {
    public Pawn(int i, int j, Colors col) : base(i, j, col) { }

    public override Bitmap WhiteOnBlack { get; } = Properties.Resources.black_white_pawn;

    public override Bitmap WhiteOnWhite { get; } = Properties.Resources.white_white_pawn;

    public override Bitmap BlackOnBlack { get; } = Properties.Resources.black_black_pawn;

    public override Bitmap BlackOnWhite { get; } = Properties.Resources.white_black_pawn;

    public override List<Figure> PossibleMoves(List<Figure> figures)
    {
      List<Figure> newFigures = new List<Figure>();

      if (Color == Colors.White && !figures.Any(x => x.Y == Y - 1 && x.X == X))
      {
        newFigures.Add(new Pawn(X, Y - 1, Colors.White));
      }
      if (Color == Colors.Black && !figures.Any(x => x.Y == Y + 1 && x.X == X))
      {
        newFigures.Add(new Pawn(X, Y + 1, Colors.Black));
      }
      if (Color == Colors.White && figures.Any(x => x.X == X + 1 && x.Y == Y - 1 && x.Color == Colors.Black && !x.IsCut))
      {
        newFigures.Add(new Pawn(X + 1, Y - 1, Colors.White));
      }
      if (Color == Colors.White && figures.Any(x => x.X == X - 1 && x.Y == Y - 1 && x.Color == Colors.Black && !x.IsCut))
      {
        newFigures.Add(new Pawn(X - 1, Y - 1, Colors.White));
      }
      if (Color == Colors.Black && figures.Any(x => x.X == X + 1 && x.Y == Y + 1 && x.Color == Colors.White && !x.IsCut))
      {
        newFigures.Add(new Pawn(X + 1, Y + 1, Colors.Black));
      }
      if (Color == Colors.Black && figures.Any(x => x.X == X - 1 && x.Y == Y + 1 && x.Color == Colors.White && !x.IsCut))
      {
        newFigures.Add(new Pawn(X - 1, Y + 1, Colors.Black));
      }

      return newFigures;
    }

    public override bool CanCut(Figure figure, List<Figure> figures)
    {
      if (Color == Colors.White && figure.Color == Colors.Black && (X == figure.X + 1 || X == figure.X - 1) && Y == figure.Y + 1)
      {
        return true;
      }
      else if (Color == Colors.Black && figure.Color == Colors.White && (X == figure.X + 1 || X == figure.X - 1) && Y == figure.Y - 1)
      {
        return true;
      }
      else
      {
        return false;
      }
    }

    public override string ToString()
    {
      return "pawn";
    }
  }
}
