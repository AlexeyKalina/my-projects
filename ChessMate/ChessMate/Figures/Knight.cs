﻿using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using ChessMate.Enums;

namespace ChessMate.Figures
{
  class Knight : Figure
  {
    public Knight(int i, int j, Colors col) : base(i, j, col) { }

    public override Bitmap WhiteOnBlack { get; } = Properties.Resources.black_white_knight;

    public override Bitmap WhiteOnWhite { get; } = Properties.Resources.white_white_knight;

    public override Bitmap BlackOnBlack { get; } = Properties.Resources.black_black_knight;

    public override Bitmap BlackOnWhite { get; } = Properties.Resources.white_black_knight;


    public override List<Figure> PossibleMoves(List<Figure> figures)
    {
      List<Figure> allMoves = new List<Figure>();

      allMoves.AddRange(MovesInOneDirection(figures, 2, 1));

      allMoves.AddRange(MovesInOneDirection(figures, 1, 2));

      allMoves.AddRange(MovesInOneDirection(figures, 2, -1));

      allMoves.AddRange(MovesInOneDirection(figures, 1, -2));

      allMoves.AddRange(MovesInOneDirection(figures, -2, 1));

      allMoves.AddRange(MovesInOneDirection(figures, -1, 2));

      allMoves.AddRange(MovesInOneDirection(figures, -2, -1));

      allMoves.AddRange(MovesInOneDirection(figures, -1, -2));

      return allMoves;
    }


    public override bool CanCut(Figure figure, List<Figure> figures)
    {
      if ((X + 2 == figure.X && Y + 1 == figure.Y ||
          X + 2 == figure.X && Y - 1 == figure.Y ||
          X - 2 == figure.X && Y + 1 == figure.Y ||
          X - 2 == figure.X && Y - 1 == figure.Y ||
          X + 1 == figure.X && Y + 2 == figure.Y ||
          X + 1 == figure.X && Y - 2 == figure.Y ||
          X - 1 == figure.X && Y + 2 == figure.Y ||
          X - 1 == figure.X && Y - 2 == figure.Y) &&
          Color != figure.Color)
      {
        return true;
      }
      else
      {
        return false;
      }
    }


    private List<Figure> MovesInOneDirection(List<Figure> figures, int dX, int dY)
    {
      List<Figure> moveInThisDirectiion = new List<ChessMate.Figure>();

      if (IsBound(X + dX, Y + dY) && !figures.Any(x => x.X == X + dX && x.Y == Y + dY && Color == x.Color && !x.IsCut))
      {
        moveInThisDirectiion.Add(new Knight(X + dX, Y + dY, Color));
      }

      return moveInThisDirectiion;
    }

    public override string ToString()
    {
      return "knight";
    }
  }
}
