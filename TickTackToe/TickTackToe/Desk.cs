﻿using System;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace TickTackToe
{
  public partial class Desk : Form
  {
    private bool isFirstGame = true;
    private bool isFirst;
    private bool isCross;
    private PictureBox[,] cells = new PictureBox[Constants.NumberOfFigures, Constants.NumberOfFigures];
    private bool[,] positions = new bool[Constants.NumberOfFigures, Constants.NumberOfFigures];

    public Desk()
    {
      InitializeComponent();
    }

    private void StartGame()
    {
      if (!isFirstGame)
      {
        for (int i = 0; i < Constants.NumberOfFigures; i++)
        {
          for (int j = 0; j < Constants.NumberOfFigures; j++)
          {
            cells[i, j].Dispose();
            positions[i, j] = false;
          }
        }
      }

      isFirstGame = false;
      CreateDesk();
      Game.StartGame(isFirst, isCross);

      if (!isFirst)
        ComputerMove();
    }

    private void CreateDesk()
    {
      Bitmap image = Properties.Resources._null;
      for (int i = 0; i < Constants.NumberOfFigures; i++)
      {
        for (int j = 0; j < Constants.NumberOfFigures; j++)
        {
          cells[i, j] = new PictureBox()
          {
            Image = image,
            Location = new Point(Constants.StartLocationX + i * Constants.SizeOfImage, Constants.StartLocationY + j * Constants.SizeOfImage),
            Name = "Cell_" + i + "_" + j,
            Size = new Size(Constants.SizeOfImage, Constants.SizeOfImage)
          };
          cells[i, j].Click += CellClick;
          Controls.Add(cells[i, j]);
        }
      }
    }

    private void ComputerMove()
    {
      Figure figure = Game.computer.Figures.Last();
      cells[figure.X, figure.Y].Image = BitmapFactory(!isCross);
      positions[figure.X, figure.Y] = true;
    }

    private void CellClick(object sender, EventArgs e)
    {
      PictureBox cell = (PictureBox)sender;
      int x = (cell.Location.X - Constants.StartLocationX) / Constants.SizeOfImage;
      int y = (cell.Location.Y - Constants.StartLocationY) / Constants.SizeOfImage;

      if (!positions[x, y])
      {
        cell.Image = BitmapFactory(isCross);
        if (Game.Move(x, y))
        {
          ComputerMove();
          MessageBox.Show(Game.winner.ToString() + " wins");
          StartGame();
          return;
        }
        ComputerMove();
        positions[x, y] = true;
      }
    }

    private static Bitmap BitmapFactory(bool isCross)
    {
      if (isCross)
      {
        return Properties.Resources.cross;
      }
      else
      {
        return Properties.Resources.nought;
      }
    }

    private void startButton_Click(object sender, EventArgs e)
    {
      StartGame();
    }

    private void radioButton1_CheckedChanged(object sender, EventArgs e)
    {
      isFirst = true;
    }

    private void radioButton2_CheckedChanged(object sender, EventArgs e)
    {
      isFirst = false;
    }

    private void radioButton3_CheckedChanged(object sender, EventArgs e)
    {
      isCross = true;
    }

    private void radioButton4_CheckedChanged(object sender, EventArgs e)
    {
      isCross = false;
    }

  }
}
