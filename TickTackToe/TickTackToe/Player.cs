﻿using System;
using System.Collections.Generic;

namespace TickTackToe
{
  abstract class Player
  {
    private bool isCross;
    private List<Figure> figures;

    public Player(bool isCross)
    {
      this.isCross = isCross;
      figures = new List<Figure>();
    }

    public bool IsCross
    {
      get { return isCross; }
      set { isCross = value; }
    }

    public List<Figure> Figures
    {
      get { return figures; }
      set { figures = value; }
    }
  }
}
