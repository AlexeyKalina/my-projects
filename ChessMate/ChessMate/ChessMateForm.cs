﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using ChessMate.Figures;
using ChessMate.Enums;

namespace ChessMate
{
  public partial class ChessMateForm : Form
  {
    readonly Dictionary<string, Bitmap> _whiteFiguresPictures = new Dictionary<string, Bitmap>()
        {
          { "pawn", Properties.Resources.black_white_pawn },
          { "queen", Properties.Resources.black_white_queen },
          { "bishop", Properties.Resources.black_white_bishop },
          { "knight", Properties.Resources.black_white_knight },
          { "king", Properties.Resources.black_white_king },
          { "rook", Properties.Resources.black_white_rook }
        };

    readonly Dictionary<string, Bitmap> _blackFiguresPictures = new Dictionary<string, Bitmap>()
        {
          { "pawn", Properties.Resources.white_black_pawn },
          { "queen", Properties.Resources.white_black_queen },
          { "bishop", Properties.Resources.white_black_bishop },
          { "knight", Properties.Resources.white_black_knight },
          { "king", Properties.Resources.white_black_king },
          { "rook", Properties.Resources.white_black_rook }
        };

    readonly Bitmap _imageBlackNull = Properties.Resources.black_null;
    readonly Bitmap _imageWhiteNull = Properties.Resources.white_null;

    bool _first;
    bool _chosen;
    int _blackChosen;
    int _whiteChosen;

    readonly PictureBox[,] _cells = new PictureBox[Constants.NumberOfCells, Constants.NumberOfCells];
    readonly PictureBox[] _whiteFigures = new PictureBox[Constants.NumberOfFigures];
    readonly PictureBox[] _blackFigures = new PictureBox[Constants.NumberOfFigures];

    readonly int[,] _positions = new int[Constants.NumberOfCells, Constants.NumberOfCells];
    int[] _figuresNabor;

    readonly List<Figure> _figures = new List<Figure>();

    public ChessMateForm()
    {
      InitializeComponent();
      _first = true;
      NewGame();
    }

    private void NewGame()
    {
      _chosen = false;
      _blackChosen = Constants.IsNotChosen;
      _whiteChosen = Constants.IsNotChosen;
      _figures.Clear();
      _figuresNabor = new[] { 8, 1, 2, 2, 1, 2, 8, 1, 2, 2, 1, 2 };

      if (!_first)
      {
        for (int i = 0; i < Constants.NumberOfFigures; i++)
        {
          _whiteFigures[i].Dispose();
          _blackFigures[i].Dispose();
        }
        for (int i = 0; i < Constants.NumberOfCells; i++)
        {
          for (int j = 0; j < Constants.NumberOfCells; j++)
          {
            _cells[i, j].Dispose();
          }
        }
      }
      _first = false;

      CreateDesk();

      CreateFigures();
    }

    private void CheckMate()
    {
      if (_figuresNabor[Constants.WhiteKingNumber] != 0 || _figuresNabor[Constants.BlackKingNumber] != 0)
      {
        MessageBox.Show("Оба короля должны присутствовать на доске");
      }
      else
      {
        switch (CheckingMate.Check(_figures))
        {
          case CheckMateResults.NoMate:
            MessageBox.Show("Мата нет");
            break;
          case CheckMateResults.Mate:
            MessageBox.Show("Мат в один ход: " + CheckingMate.mateMove.Figure + " " + (char)(CheckingMate.mateMove.X1 + 'a') + "" + (CheckingMate.mateMove.Y1 + 1) + " на " + (char)(CheckingMate.mateMove.X2 + 'a') + "" + (CheckingMate.mateMove.Y2 + 1));
            break;
          case CheckMateResults.MateWithChangePawn:
            MessageBox.Show("Мат в один ход: " + CheckingMate.mateMove.Figure + " " + (char)(CheckingMate.mateMove.X1 + 'a') + "" + (CheckingMate.mateMove.Y1 + 1) + " на " + (char)(CheckingMate.mateMove.X2 + 'a') + "" + (CheckingMate.mateMove.Y2 + 1) + " с заменой на ферзя или на коня");
            break;
          case CheckMateResults.KingIsAttacked:
            MessageBox.Show("Король находится под ударом");
            break;
        }
      }
    }

    private void CreateFigures()
    {
      for (int i = 0; i < Constants.NumberOfFigures; i++)
      {
        _whiteFigures[i] = new PictureBox()
        {
          Image = _whiteFiguresPictures.Values.ToArray()[i],
          Location = new Point(10, 70 + i * 45),
          Name = "white " + _whiteFiguresPictures.Keys.ToArray()[i],
          Size = new Size(Constants.SizeOfImage, Constants.SizeOfImage)
        };
        _whiteFigures[i].Click += FigureClick;
        Border(_whiteFigures[i], Constants.White);
        Controls.Add(_whiteFigures[i]);

        _blackFigures[i] = new PictureBox()
        {
          Image = _blackFiguresPictures.Values.ToArray()[i],
          Location = new Point(410, 70 + i * 45),
          Name = "black " + _blackFiguresPictures.Keys.ToArray()[i],
          Size = new Size(Constants.SizeOfImage, Constants.SizeOfImage)
        };
        _blackFigures[i].Click += FigureClick;
        Border(_blackFigures[i], Constants.Black);
        Controls.Add(_blackFigures[i]);
      }
    }

    private void CreateDesk()
    {
      Label[] letters = new Label[Constants.NumberOfCells];
      Label[] numbers = new Label[Constants.NumberOfCells];

      for (int i = 0; i < Constants.NumberOfCells; i++)
      {
        letters[i] = new Label
        {
          Location = new Point(85 + i * Constants.SizeOfImage, 390),
          Text = ((char)(i + 'a')).ToString(),
          Name = "Letter_" + ((char)(i + 'a')).ToString(),
          Size = new Size(10, 15)
        };
        Controls.Add(letters[i]);

        numbers[i] = new Label
        {
          Location = new Point(60, 85 + i * Constants.SizeOfImage),
          Text = (i + 1).ToString(),
          Name = "Number_" + (i + 1).ToString(),
          Size = new Size(10, 15)
        };
        Controls.Add(numbers[i]);

        for (int j = 0; j < Constants.NumberOfCells; j++)
        {
          _cells[i, j] = new PictureBox()
          {
            Name = "Cell_" + i.ToString() + "_" + j.ToString(),
            Location = new Point(70 + i * Constants.SizeOfImage, 70 + j * Constants.SizeOfImage),
            Size = new Size(Constants.SizeOfImage, Constants.SizeOfImage)
          };
          _cells[i, j].Click += CellClick;
          CreateCell(i, j);
          Controls.Add(_cells[i, j]);
        }
      }
    }

    private void CreateCell(int i, int j)
    {
      if (i % 2 == 0 && j % 2 == 0 || i % 2 != 0 && j % 2 != 0)
      {
        _cells[i, j].Image = _imageWhiteNull;
        _positions[i, j] = Constants.FreeWhiteCell;
      }
      else
      {
        _cells[i, j].Image = _imageBlackNull;
        _positions[i, j] = Constants.FreeBlackCell;
      }
    }

    private void FigureClick(object sender, EventArgs e)
    {
      PictureBox p = (PictureBox)sender;
      int i = (p.Location.X - 70) / 40;
      int j = (p.Location.Y - 70) / 45;

      // Кликаем на фигуру, все экземпляры которой уже на доске
      if (i == -1 && _figuresNabor[j] == 0 || i == Constants.NumberOfCells && _figuresNabor[j + Constants.NumberOfFigures] == 0)
      {
        return;
      }

      // Кликаем на белую фигуру, которая не была выбрана перед этим
      if (_chosen && i == -1 && j != _whiteChosen)
      {
        // Если до этого не была выбрана другая белая, убрать выделение с черной выбранной фигуры
        if (_whiteChosen == Constants.IsNotChosen)
        {
          Border(_blackFigures[_blackChosen], Constants.Black);
          _blackChosen = Constants.IsNotChosen;
        }
        // Иначе убрать выделение с белой выбранной фигуры
        else
        {
          Border(_whiteFigures[_whiteChosen], Constants.White);
        }
        // Выделить фигуру на которую кликнули
        _whiteChosen = j;
        Border(p, Color.Blue);
      }

      // Кликаем на черную фигуру, которая не была выбрана перед этим
      else if (_chosen && i == 8 && j != _blackChosen)
      {
        // Если до этого не была выбрана другая черная, убрать выделение с белой выбранной фигуры
        if (_blackChosen == Constants.IsNotChosen)
        {
          Border(_whiteFigures[_whiteChosen], Constants.White);
          _whiteChosen = Constants.IsNotChosen;
        }
        // Иначе убрать выделение с черной выбранной фигуры
        else
        {
          Border(_blackFigures[_blackChosen], Constants.Black);
        }
        // Выделить фигуру на которую кликнули
        _blackChosen = j;
        Border(p, Color.Blue);
      }

      // Кликаем на фигуру, которая и так выбрана
      else if (_chosen)
      {
        // Убрать выделение фигуры
        if (_whiteChosen != Constants.IsNotChosen)
        {
          Border(p, Constants.White);
        }
        if (_blackChosen != Constants.IsNotChosen)
        {
          Border(p, Constants.Black);
        }
        _chosen = false;
        _whiteChosen = Constants.IsNotChosen;
        _blackChosen = Constants.IsNotChosen;
      }

      // Кликаем на фигуру, при этом никакая фигура перед этим не была выбрана
      else
      {
        // Выделяем эту фигуру
        Border(p, Color.Blue);
        _chosen = true;
        if (i == -1)
        {
          _whiteChosen = j;
          _blackChosen = Constants.IsNotChosen;
        }
        else
        {
          _blackChosen = j;
          _whiteChosen = Constants.IsNotChosen;
        }
      }
    }

    private void Border(PictureBox p, Color col)
    {
      Graphics g = Graphics.FromImage(p.Image);
      g.DrawRectangle(new Pen(col, 3), 0, 0, 38, 38);
      p.Invalidate();
    }

    private void CellClick(object sender, EventArgs e)
    {
      PictureBox p = (PictureBox)sender;
      int i = (p.Location.X - 70) / Constants.SizeOfImage;
      int j = (p.Location.Y - 70) / Constants.SizeOfImage;

      // Никакая фигура не выбрана и мы кликаем на фигуру стоящую на доске
      if (!_chosen && _positions[i, j] >= 0)
      {
        // Удаляем фигуру с доски
        _figuresNabor[_positions[i, j]]++;
        _figures.RemoveAll(x => x.X == i && x.Y == j);

        if (_positions[i, j] >= Constants.NumberOfFigures)
        {
          Border(_blackFigures[_positions[i, j] - Constants.NumberOfFigures], Constants.Black);
        }
        else
        {
          Border(_whiteFigures[_positions[i, j]], Constants.White);
        }
        CreateCell(i, j);

        return;
      }

      // Какая-то фигура выбрана и мы кликаем на фигуру стоящую на доске
      else if (_positions[i, j] >= 0)
      {
        return;
      }

      // Выбрана белая фигура и мы кликаем на свободную клетку доски
      else if (_whiteChosen != Constants.IsNotChosen)
      {
        _figuresNabor[_whiteChosen]--;
        Border(_whiteFigures[_whiteChosen], Constants.White);

        if (_figuresNabor[_whiteChosen] == 0)
        {
          Border(_whiteFigures[_whiteChosen], Color.Black);
        }

        AddFigureToDesk(ref _whiteChosen, i, j, Colors.White);
      }

      // Выбрана черная фигура и мы кликаем на свободную клетку доски
      else if (_blackChosen != Constants.IsNotChosen)
      {
        _figuresNabor[_blackChosen + Constants.NumberOfFigures]--;
        Border(_blackFigures[_blackChosen], Constants.Black);

        if (_figuresNabor[_blackChosen + Constants.NumberOfFigures] == 0)
        {
          Border(_blackFigures[_blackChosen], Color.Black);
        }

        AddFigureToDesk(ref _blackChosen, i, j, Colors.Black);
      }
    }

    private void AddFigureToDesk(ref int chosen, int i, int j, Colors color)
    {
      switch (chosen)
      {
        case 0:
          AddFigureToDesk(new Pawn(i, j, color));
          break;
        case 1:
          AddFigureToDesk(new Queen(i, j, color));
          break;
        case 2:
          AddFigureToDesk(new Bishop(i, j, color));
          break;
        case 3:
          AddFigureToDesk(new Knight(i, j, color));
          break;
        case 4:
          AddFigureToDesk(new King(i, j, color));
          break;
        case 5:
          AddFigureToDesk(new Rook(i, j, color));
          break;
      }

      _positions[i, j] = color == Colors.White ? chosen : chosen + Constants.NumberOfFigures;
      _chosen = false;
      chosen = Constants.IsNotChosen;
    }

    private void AddFigureToDesk(Figure figure)
    {
      _figures.Add(figure);
      if (figure.Color == Colors.White)
      {
        _cells[figure.X, figure.Y].Image = _positions[figure.X, figure.Y] == Constants.FreeBlackCell ? figure.WhiteOnBlack : figure.WhiteOnWhite;
      }
      else
      {
        _cells[figure.X, figure.Y].Image = _positions[figure.X, figure.Y] == Constants.FreeBlackCell ? figure.BlackOnBlack : figure.BlackOnWhite;
      }
    }

    private void newGameMenuItem_Click(object sender, EventArgs e)
    {
      NewGame();
    }

    private void CheckMateMenuItem_Click(object sender, EventArgs e)
    {
      CheckMate();
    }

    private void checkMateButton_Click(object sender, EventArgs e)
    {
      CheckMate();
    }

    private void exitMenuItem_Click(object sender, EventArgs e)
    {
      Close();
    }

    private void aboutProgrameMenuItem_Click(object sender, EventArgs e)
    {
      MessageBox.Show("ChessMate. Калина Алексей. 2016", "О программе");
    }
  }
}
