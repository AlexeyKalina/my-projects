﻿using System.Collections.Generic;

namespace TickTackToe
{
  static class Game
  {
    public static Person person;
    public static Computer computer;
    public static Player winner;
    public static int[,] veights;

    public static void StartGame(bool isFirst, bool isCross)
    {
      person = new Person(isCross);
      computer = new Computer(!isCross);
      veights = new int[Constants.NumberOfFigures, Constants.NumberOfFigures];

      for (int i = 0; i < Constants.NumberOfFigures; i++)
      {
        for (int j = 0; j < Constants.NumberOfFigures; j++)
        {
          veights[i, j] = 0;
        }
      }

      if (!isFirst)
      {
        computer.Go();
      }
    }

    public static bool Move(int x, int y)
    {
      person.Go(x, y);

      if (IsEnd(person.Figures))
      {
        winner = person;
        return true;
      }

      computer.Go();

      if (IsEnd(computer.Figures))
      {
        winner = computer;
        return true;
      }

      return false;
    }

    public static bool IsEnd(List<Figure> figures)
    {
      foreach (Figure figure in figures)
      {
        if (IsEndInOneDirection(figures, figure, 0, 1)
          || IsEndInOneDirection(figures, figure, 1, 0)
          || IsEndInOneDirection(figures, figure, 1, 1)
          || IsEndInOneDirection(figures, figure, 1, -1))
        {
          return true;
        }
      }
      return false;
    }

    private static bool IsEndInOneDirection(List<Figure> figures, Figure thisFigure, int dX, int dY)
    {
      int count = 0;

      while (!thisFigure.Equals(default(Figure)))
      {
        count++;
        thisFigure = figures.Find(f => f.X == thisFigure.X + dX && f.Y == thisFigure.Y + dY);
      }

      if (count == 5)
      {
        return true;
      }
      else
      {
        return false;
      }
    }
  }
}
