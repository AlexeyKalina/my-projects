﻿using System.Collections.Generic;

namespace TickTackToe
{
  class Constants
  {
    public const int NumberOfFigures = 15;
    public const int CountToWin = 5;

    public const int SizeOfImage = 20;
    public const int StartLocationX = 30;
    public const int StartLocationY = 30;

    public static Dictionary<string, int> Variants = new Dictionary<string, int>
      {
        {"00000", 1},
        {"00001", 2},
        {"00010", 2},
        {"00011", 200},
        {"00100", 2},
        {"00101", 200},
        {"00110", 200},
        {"00111", 20000},
        {"01000", 2},
        {"01001", 200},
        {"01010", 200},
        {"01011", 20000},
        {"01100", 200},
        {"01101", 20000},
        {"01110", 20000},
        {"01111", 2000000},
        {"10000", 2},
        {"10001", 200},
        {"10010", 200},
        {"10011", 20000},
        {"10100", 200},
        {"10101", 20000},
        {"10110", 20000},
        {"10111", 2000000},
        {"11000", 200},
        {"11001", 20000},
        {"11010", 20000},
        {"11011", 2000000},
        {"11100", 20000},
        {"11101", 2000000},
        {"11110", 2000000}
      };
  }
}
